package com.prase.mt.lab.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.prase.mt.lab.client.PhraseApiClient;
import com.prase.mt.lab.client.PhraseProjectDto;
import com.prase.mt.lab.domain.PhraseAccountEntity;
import com.prase.mt.lab.execption.EntityNotFoundException;
import com.prase.mt.lab.repository.PhraseAccountRepository;
import com.prase.mt.lab.service.PhraseAccountService;

@ExtendWith(MockitoExtension.class)
class PhraseAccountServiceImplTest {

    @Mock
    private PhraseAccountRepository accountRepository;
    @Mock
    private PhraseApiClient client;

    private PhraseAccountService service;

    @BeforeEach
    void setUp() {
        service = new PhraseAccountServiceImpl(accountRepository, client);
    }

    @Nested
    class GetAllAccountsTest {
        @Test
        void ok() {
            doReturn(List.of(getEntity())).when(accountRepository).findAll();
            assertThat(service.getAllAccounts()).hasSize(1);
        }
    }

    @Nested
    class GetAccountByIdTest {
        @Test
        void ok() {
            doReturn(Optional.of(getEntity())).when(accountRepository).findById(1L);
            assertThat(service.getAccountById(1L)).isNotNull();
        }

        @Test
        void wrongIdThrowException() {
            doReturn(Optional.empty()).when(accountRepository).findById(1L);
            assertThatThrownBy(() -> service.getAccountById(1L))
                    .isInstanceOf(EntityNotFoundException.class);
        }
    }

    @Nested
    class CreateAccountTest {

        @Test
        void ok() {
            doAnswer((a) -> a.getArgument(0)).when(accountRepository).save(any());

            service.createAccount("newName", "newPass");

            ArgumentCaptor<PhraseAccountEntity> captor = ArgumentCaptor.forClass(PhraseAccountEntity.class);
            verify(accountRepository).save(captor.capture());

            assertThat(captor.getValue().getUsername()).isEqualTo("newName");
            assertThat(captor.getValue().getPassword()).isEqualTo("newPass");
        }

        @Test
        void emptyPasswordThrowException() {
            assertThatThrownBy(() -> service.createAccount("user", "  "))
                    .isInstanceOf(IllegalArgumentException.class)
                    .hasMessageContaining("Password");
        }

        @Test
        void emptyUsernameThrowException() {
            assertThatThrownBy(() -> service.createAccount(" ", "pass"))
                    .isInstanceOf(IllegalArgumentException.class)
                    .hasMessageContaining("Username");
        }
    }


    @Nested
    class UpdateAccountTest {

        @Test
        void ok() {
            doReturn(Optional.of(getEntity())).when(accountRepository).findById(1L);

            service.updateAccount(1L, "newName", "newPass");

            ArgumentCaptor<PhraseAccountEntity> captor = ArgumentCaptor.forClass(PhraseAccountEntity.class);
            verify(accountRepository).save(captor.capture());

            assertThat(captor.getValue().getUsername()).isEqualTo("newName");
            assertThat(captor.getValue().getPassword()).isEqualTo("newPass");
        }

        @Test
        void emptyPasswordThrowException() {
            doReturn(Optional.of(getEntity())).when(accountRepository).findById(1L);

            assertThatThrownBy(() -> service.updateAccount(1L, "user", "  "))
                    .isInstanceOf(IllegalArgumentException.class)
                    .hasMessageContaining("Password");
        }

        @Test
        void emptyUsernameThrowException() {
            doReturn(Optional.of(getEntity())).when(accountRepository).findById(1L);

            assertThatThrownBy(() -> service.updateAccount(1L, " ", "pass"))
                    .isInstanceOf(IllegalArgumentException.class)
                    .hasMessageContaining("Username");
        }
    }


    @Nested
    class GetAccountProjectsTest {

        @Test
        void ok() {
            PhraseAccountEntity entity = getEntity();
            doReturn(Optional.of(entity)).when(accountRepository).findById(1L);
            doReturn("token").when(client).getPhraseToken(entity);

            service.getAccountProjects(1L, false);

            verify(client).getPhraseToken(entity);
            verify(client).getAllProjects("token", false);
        }

        @Test
        void wrongIdThrowException() {
            doReturn(Optional.empty()).when(accountRepository).findById(1L);
            assertThatThrownBy(() -> service.getAccountProjects(1L, false))
                    .isInstanceOf(EntityNotFoundException.class);
        }
    }
    private PhraseAccountEntity getEntity() {
        PhraseAccountEntity account = new PhraseAccountEntity("name", "pass");
        account.setId(1L);
        return account;
    }
}