package com.prase.mt.lab.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import com.prase.mt.lab.domain.PhraseAccountEntity;
import com.prase.mt.lab.service.PhraseAccountService;

@ActiveProfiles("test")
@SpringBootTest
@Sql({"/db/migration/test_data.sql"})
@Transactional
class PhraseAccountServiceImplIntegrationTest {

    @Autowired
    private PhraseAccountService service;

    @Test
    void getAllAccountTest() {
        List<PhraseAccountEntity> allAccounts = service.getAllAccounts();
        assertThat(allAccounts).isNotEmpty();
    }

    @Test
    void getAccountByIdTest() {
        assertThat(service.getAccountById(100L).getId()).isEqualTo(100L);
    }

    @Test
    void createAccountTest() {
        assertThat(service.createAccount("newUser", "newPass").getId()).isNotNull();
    }

    @Test
    void updateAccountTest() {
        service.updateAccount(103L, "user42", "42");
        PhraseAccountEntity account = service.getAccountById(103L);
        assertThat(account.getUsername()).isEqualTo("user42");
        assertThat(account.getPassword()).isEqualTo("42");
    }
}