package com.prase.mt.lab.api.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.prase.mt.lab.api.converter.PhraseAccountConverter;
import com.prase.mt.lab.client.PhraseProjectDto;
import com.prase.mt.lab.client.exception.PhraseAccountLoginException;
import com.prase.mt.lab.client.exception.PhraseGenericClientException;
import com.prase.mt.lab.domain.PhraseAccountEntity;
import com.prase.mt.lab.execption.EntityNotFoundException;
import com.prase.mt.lab.service.PhraseAccountService;

import jakarta.persistence.OptimisticLockException;

@WebMvcTest(PhraseAccountRestController.class)
class PhraseAccountRestControllerTest {
    private static final String BASE_URL = "/phrase-demo/account";

    @Autowired
    private MockMvc mvc;
    @MockBean
    private PhraseAccountService service;
    @SpyBean
    private PhraseAccountConverter converter;

    @Nested
    class GetAllAccountsTest {

        @Test
        void ok() throws Exception {
            PhraseAccountEntity account = new PhraseAccountEntity("name", "pass");
            account.setId(42L);
            doReturn(List.of(account)).when(service).getAllAccounts();

            mvc.perform(get(BASE_URL).contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(1)))
                    .andExpect(jsonPath("$[0].id").value(42L))
                    .andExpect(jsonPath("$[0].username").value("name"))
                    .andExpect(jsonPath("$[0].password").value("pass"))
            ;
        }
    }

    @Nested
    class GetAccountByIdTest {

        @Test
        void ok() throws Exception {
            PhraseAccountEntity account = new PhraseAccountEntity("name", "pass");
            account.setId(42L);
            doReturn(account).when(service).getAccountById(42L);

            mvc.perform(get(BASE_URL + "/42").contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").value(42L))
                    .andExpect(jsonPath("$.username").value("name"))
                    .andExpect(jsonPath("$.password").value("pass"))
            ;
        }

        @Test
        void wrongId() throws Exception {
            doThrow(new EntityNotFoundException("Entity not found")).when(service).getAccountById(42L);

            mvc.perform(get(BASE_URL + "/42").contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isNotFound())
                    .andExpect(jsonPath("$.code").value(404))
                    .andExpect(jsonPath("$.message").value("Entity not found"))
            ;
        }
    }

    @Nested
    class CreateAccountTest {

        @Test
        void ok() throws Exception {
            PhraseAccountEntity account = new PhraseAccountEntity("name", "pass");
            account.setId(42L);
            doReturn(account).when(service).createAccount("name", "pass");

            mvc.perform(post(BASE_URL)
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{ \"username\": \"name\",  \"password\": \"pass\"}")
                    ).andExpect(status().isCreated())
                    .andExpect(jsonPath("$.id").value(42L))
                    .andExpect(jsonPath("$.username").value("name"))
                    .andExpect(jsonPath("$.password").value("pass"))
            ;
        }

        @Test
        void emptyFieldValue() throws Exception {
            mvc.perform(put(BASE_URL + "/42")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{ \"username\": \"\",  \"password\": \" \"}")
                    ).andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.code").value(400))
                    .andExpect(jsonPath("$.fields", hasSize(2)))
            ;
        }
    }

    @Nested
    class UpdateAccountTest {

        @Test
        void ok() throws Exception {
            mvc.perform(put(BASE_URL + "/42")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{ \"username\": \"name\",  \"password\": \"pass\"}")
            ).andExpect(status().isNoContent())
            ;
            verify(service).updateAccount(42L, "name", "pass");
        }

        @Test
        void wrongId() throws Exception {
            doThrow(new EntityNotFoundException("Entity not found")).when(service).updateAccount(42L, "name", "pass");

            mvc.perform(put(BASE_URL + "/42")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{ \"username\": \"name\",  \"password\": \"pass\"}")
                    ).andExpect(status().isNotFound())
                    .andExpect(jsonPath("$.code").value(404))
                    .andExpect(jsonPath("$.message").value("Entity not found"))
            ;
        }

        @Test
        void optimisticLockException() throws Exception {
            doThrow(new OptimisticLockException("Entity lock")).when(service).updateAccount(42L, "name", "pass");

            mvc.perform(put(BASE_URL + "/42")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{ \"username\": \"name\",  \"password\": \"pass\"}")
                    ).andExpect(status().isConflict())
                    .andExpect(jsonPath("$.code").value(409))
                    .andExpect(jsonPath("$.message").value("Entity lock"))
            ;
        }

        @Test
        void emptyFieldValue() throws Exception {
            mvc.perform(put(BASE_URL + "/42")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content("{ \"username\": \"\",  \"password\": \" \"}")
                    ).andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.code").value(400))
                    .andExpect(jsonPath("$.fields", hasSize(2)))
            ;
        }
    }

    @Nested
    class GetAccountProjectsTest {

        @Test
        void ok() throws Exception {
            doReturn(List.of(new PhraseProjectDto("test", "NEW", "CZ", List.of("SK", "PL"))))
                    .when(service).getAccountProjects(42L, false);

            mvc.perform(get(BASE_URL + "/42/projects")
                            .contentType(MediaType.APPLICATION_JSON)
                    ).andExpect(status().isOk())
                    .andExpect(jsonPath("$", hasSize(1)))
                    .andExpect(jsonPath("$[0].name").value("test"))
                    .andExpect(jsonPath("$[0].status").value("NEW"))
                    .andExpect(jsonPath("$[0].sourceLang").value("CZ"))
                    .andExpect(jsonPath("$[0].targetLangs", hasSize(2)))
            ;
        }

        @Test
        void phraseGenericClientException() throws Exception {
            doThrow(new PhraseGenericClientException("ClientError")).when(service).getAccountProjects(42L, false);

            mvc.perform(get(BASE_URL + "/42/projects").contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isInternalServerError())
                    .andExpect(jsonPath("$.code").value(500))
                    .andExpect(jsonPath("$.message").value("ClientError"))
            ;
        }


        @Test
        void phraseAccountLoginException() throws Exception {
            doThrow(new PhraseAccountLoginException("ClientError", null)).when(service).getAccountProjects(42L, false);

            mvc.perform(get(BASE_URL + "/42/projects").contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.code").value(400))
                    .andExpect(jsonPath("$.message").value("ClientError"))
            ;
        }
    }
}