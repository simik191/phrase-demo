package com.prase.mt.lab.client;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withRawStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withUnauthorizedRequest;

import java.util.List;

import org.assertj.core.groups.Tuple;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.client.MockRestServiceServer;

import com.prase.mt.lab.PhraseProperties;
import com.prase.mt.lab.client.exception.PhraseAccountLoginException;
import com.prase.mt.lab.client.exception.PhraseGenericClientException;
import com.prase.mt.lab.domain.PhraseAccountEntity;

@ActiveProfiles("test")
@RestClientTest(PhraseApiClient.class)
class PhraseApiClientTest {

    @SpyBean
    private PhraseProperties properties;
    @Autowired
    private PhraseApiClient client;
    @Autowired
    private MockRestServiceServer mockRestServiceServer;

    @Nested
    class LoginTest {

        @Test
        void ok() {
            mockRestServiceServer
                    .expect(requestTo("http://test.cz/api2/v1/auth/login"))
                    .andRespond(withSuccess("""
                                            {
                                            "user": {
                                            "firstName": "string",
                                            "lastName": "string",
                                            "userName": "string",
                                            "email": "string",
                                            "role": "ADMIN",
                                            "id": "string",
                                            "uid": "string"
                                            },
                                            "token": "token123",
                                            "expires": "2019-08-24T14:15:22Z",
                                            "lastInvalidateAllSessionsPerformed": "2019-08-24T14:15:22Z"
                                            }""", MediaType.APPLICATION_JSON));

            String phraseToken = client.getPhraseToken(new PhraseAccountEntity("user", "pass"));
            assertThat(phraseToken).isEqualTo("token123");
        }

        @Test
        void return401() {
            mockRestServiceServer
                    .expect(requestTo("http://test.cz/api2/v1/auth/login"))
                    .andRespond(withUnauthorizedRequest());

            PhraseAccountEntity account = new PhraseAccountEntity("user", "pass");
            assertThatThrownBy(() -> client.getPhraseToken(account)).isInstanceOf(PhraseAccountLoginException.class);
        }

        @Test
        void return500() {
            mockRestServiceServer
                    .expect(requestTo("http://test.cz/api2/v1/auth/login"))
                    .andRespond(withRawStatus(500));

            PhraseAccountEntity account = new PhraseAccountEntity("user", "pass");
            assertThatThrownBy(() -> client.getPhraseToken(account)).isInstanceOf(PhraseGenericClientException.class);
        }
    }

    @Nested
    class ListAllProjectTest {

        @Test
        void ok() {
            mockRestServiceServer
                    .expect(requestTo("http://test.cz/api2/v1/projects?pageNumber=0&pageSize=1&includeArchived=false"))
                    .andRespond(withSuccess("""
                                            {  "totalElements": 2,
                                              "totalPages": 2,
                                              "pageSize": 1,
                                              "pageNumber": 0,
                                              "numberOfElements": 1,
                                              "content": [
                                                {
                                                  "name" : "project1",
                                                  "sourceLang": "CZ",
                                                  "targetLangs": [
                                                    "SK"
                                                  ],
                                                  "userRole": "string"
                                                }
                                              ]
                                            }""", MediaType.APPLICATION_JSON));
            mockRestServiceServer
                    .expect(requestTo("http://test.cz/api2/v1/projects?pageNumber=1&pageSize=1&includeArchived=false"))
                    .andRespond(withSuccess("""
                                            {  "totalElements": 2,
                                              "totalPages": 2,
                                              "pageSize": 1,
                                              "pageNumber": 1,
                                              "numberOfElements": 1,
                                              "content": [
                                                {
                                                  "name" : "project2",
                                                  "sourceLang": "PL",
                                                  "targetLangs": [
                                                    "CZ"
                                                  ],
                                                  "userRole": "string"
                                                }
                                              ]
                                            }""", MediaType.APPLICATION_JSON));

            List<PhraseProjectDto> projectDtos = client.getAllProjects("token", false);
            assertThat(projectDtos).extracting(
                    PhraseProjectDto::name,
                    PhraseProjectDto::sourceLang
            ).containsExactly(
                    Tuple.tuple("project1", "CZ"),
                    Tuple.tuple("project2", "PL")
            );
        }

        @Test
        void return500() {
            mockRestServiceServer
                    .expect(requestTo("http://test.cz/api2/v1/projects?pageNumber=0&pageSize=1&includeArchived=false"))
                    .andRespond(withRawStatus(500));

            assertThatThrownBy(() -> client.getAllProjects("token", false)).isInstanceOf(PhraseGenericClientException.class);
        }
    }


}