package com.prase.mt.lab.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.prase.mt.lab.client.PhraseApiClient;
import com.prase.mt.lab.client.PhraseProjectDto;
import com.prase.mt.lab.domain.PhraseAccountEntity;
import com.prase.mt.lab.execption.EntityNotFoundException;
import com.prase.mt.lab.repository.PhraseAccountRepository;
import com.prase.mt.lab.service.PhraseAccountService;

@Service
@Transactional(readOnly = true)
public class PhraseAccountServiceImpl implements PhraseAccountService {

    private final PhraseAccountRepository accountRepository;
    private final PhraseApiClient client;

    public PhraseAccountServiceImpl(PhraseAccountRepository accountRepository, PhraseApiClient client) {
        this.accountRepository = accountRepository;
        this.client = client;
    }

    @Override
    public List<PhraseAccountEntity> getAllAccounts() {
        return accountRepository.findAll();
    }

    @Override
    public PhraseAccountEntity getAccountById(Long id) {
        return accountRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Phrase account with id " + id + " was not found"));
    }

    @Override
    @Transactional
    public PhraseAccountEntity createAccount(String username, String password) {
        assertAccountParameters(username, password);
        return accountRepository.save(new PhraseAccountEntity(username, password));
    }

    @Override
    @Transactional
    public void updateAccount(Long id, String username, String password) {
        PhraseAccountEntity account = getAccountById(id);
        assertAccountParameters(username, password);
        account.setUsername(username);
        account.setPassword(password);
        accountRepository.save(account);
    }

    @Override
    public List<PhraseProjectDto> getAccountProjects(Long accountId, boolean includeArchived) {
        PhraseAccountEntity account = getAccountById(accountId);
        String phraseToken = client.getPhraseToken(account);
        return client.getAllProjects(phraseToken, includeArchived);
    }

    private static void assertAccountParameters(String username, String password) {
        if (StringUtils.isBlank(username)) {
            throw new IllegalArgumentException("Username parameter can not be blank");
        }
        if (StringUtils.isBlank(password)) {
            throw new IllegalArgumentException("Password parameter can not be blank");
        }
    }
}
