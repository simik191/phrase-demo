package com.prase.mt.lab.service;

import java.util.List;

import com.prase.mt.lab.client.PhraseProjectDto;
import com.prase.mt.lab.domain.PhraseAccountEntity;
import com.prase.mt.lab.execption.EntityNotFoundException;

import jakarta.persistence.OptimisticLockException;

public interface PhraseAccountService {

    /**
     * Gets all phrase account
     *
     * @return all accounts
     */
    List<PhraseAccountEntity> getAllAccounts();

    /**
     * Gets phrase account with given id
     *
     * @param id Phrase account id
     * @return Phrase account
     * @throws EntityNotFoundException If phrase account with given id was not found
     */
    PhraseAccountEntity getAccountById(Long id);

    /**
     * Creates new phrase account
     *
     * @param username username
     * @param password password
     * @return New phrase account
     * @throws IllegalArgumentException If any account field is empty
     */
    PhraseAccountEntity createAccount(String username, String password);

    /**
     * Updates phrase account
     *
     * @param id       Phrase account id
     * @param username username
     * @param password password
     * @throws IllegalArgumentException If any account field is empty
     * @throws EntityNotFoundException  If phrase account with given id was not found
     * @throws OptimisticLockException In case an optimistic locking conflict occurs for phrase account with given id
     */
    void updateAccount(Long id, String username, String password);

    /**
     * Gets all project for account
     *
     * @param accountId Account id
     * @param includeArchived List also archived projects
     * @return All projects
     */
    List<PhraseProjectDto> getAccountProjects(Long accountId, boolean includeArchived);
}
