package com.prase.mt.lab.execption;

/**
 * Exception use when entity with given id can not be found in db
 */
public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(String message) {
        super(message);
    }
}
