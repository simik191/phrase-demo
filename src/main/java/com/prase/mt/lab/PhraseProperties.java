package com.prase.mt.lab;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Validated
@Component
@ConfigurationProperties("phrase")
public class PhraseProperties {
    @NotBlank
    private String url;

    @NotNull
    @Min(1)
    private Long projectPageSize;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getProjectPageSize() {
        return projectPageSize;
    }

    public void setProjectPageSize(Long projectPageSize) {
        this.projectPageSize = projectPageSize;
    }
}
