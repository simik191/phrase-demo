package com.prase.mt.lab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MtLabDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MtLabDemoApplication.class, args);
	}

}
