package com.prase.mt.lab.api.converter;

import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

import com.prase.mt.lab.api.dto.PhraseAccountDto;
import com.prase.mt.lab.domain.PhraseAccountEntity;

/**
 * Phrase account converter
 */
@Component
public class PhraseAccountConverter {

    public PhraseAccountDto createDto(PhraseAccountEntity entity) {
        return new PhraseAccountDto(entity.getId(), entity.getUsername(), entity.getPassword());
    }

    public List<PhraseAccountDto> createDTOList(List<PhraseAccountEntity> entity) {
        if (entity == null) {
            return Collections.emptyList();
        }

        return entity.stream()
                .map(this::createDto)
                .toList();
    }
}
