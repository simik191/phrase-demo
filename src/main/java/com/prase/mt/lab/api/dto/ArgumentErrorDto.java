package com.prase.mt.lab.api.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;

public class ArgumentErrorDto extends ErrorDto {

    private final List<FieldErrorDto> fields;

    public ArgumentErrorDto(HttpStatus status, String message) {
            super(status, message);
        this.fields = new ArrayList<>();
    }

    public void addFieldError(String field, String message) {
        this.fields.add(new FieldErrorDto(field, message));
    }

    public List<FieldErrorDto> getFields() {
        return fields;
    }

    private record FieldErrorDto(String field, String message) {

    }
}
