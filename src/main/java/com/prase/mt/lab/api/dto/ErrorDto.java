package com.prase.mt.lab.api.dto;

import org.springframework.http.HttpStatus;

public class ErrorDto {

    private final long code;
    private final String message;

    public ErrorDto(HttpStatus status, String message) {
        this.code = status.value();
        this.message = message;
    }

    public long getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
