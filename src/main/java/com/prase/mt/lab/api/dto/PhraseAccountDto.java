package com.prase.mt.lab.api.dto;

public record PhraseAccountDto(Long id, String username, String password) {
}
