package com.prase.mt.lab.api.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.prase.mt.lab.api.dto.ArgumentErrorDto;
import com.prase.mt.lab.api.dto.ErrorDto;
import com.prase.mt.lab.client.exception.PhraseAccountLoginException;
import com.prase.mt.lab.client.exception.PhraseGenericClientException;
import com.prase.mt.lab.execption.EntityNotFoundException;

import jakarta.persistence.OptimisticLockException;

/**
 * Global exception handler
 */
@ControllerAdvice
public class PhraseExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(PhraseExceptionHandler.class);

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDto handleEntityNotFoundException(EntityNotFoundException exception) {
        return logAndCreateErrorDto(HttpStatus.NOT_FOUND, exception);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleEntityNotFoundException(IllegalArgumentException exception) {
        return logAndCreateErrorDto(HttpStatus.BAD_REQUEST, exception);
    }

    @ExceptionHandler(OptimisticLockException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.CONFLICT)
    public ErrorDto handleOptimisticLockException(OptimisticLockException exception) {
        return logAndCreateErrorDto(HttpStatus.CONFLICT, exception);
    }

    @ExceptionHandler(PhraseAccountLoginException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handlePhraseAccountLoginException(PhraseAccountLoginException exception) {
        return logAndCreateErrorDto(HttpStatus.BAD_REQUEST, exception);
    }

    @ExceptionHandler(PhraseGenericClientException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto handlePhraseGenericClientException(PhraseGenericClientException exception) {
        return logAndCreateErrorDto(HttpStatus.INTERNAL_SERVER_ERROR, exception);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ArgumentErrorDto methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        logError(HttpStatus.BAD_REQUEST, ex);
        return processFieldErrors(fieldErrors);
    }

    private ArgumentErrorDto processFieldErrors(List<org.springframework.validation.FieldError> fieldErrors) {
        ArgumentErrorDto error = new ArgumentErrorDto(HttpStatus.BAD_REQUEST, "validation error");
        for (org.springframework.validation.FieldError fieldError : fieldErrors) {
            error.addFieldError(fieldError.getField(), fieldError.getDefaultMessage());
        }
        return error;
    }

    @ExceptionHandler(Throwable.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto handleThrowable(Throwable exception) {
        return logAndCreateErrorDto(HttpStatus.INTERNAL_SERVER_ERROR, exception);
    }

    private void logError(HttpStatus status, Throwable exception) {
        if (logger.isDebugEnabled()) {
            logger.info("action=exceptionHandler error={} result={}", status, exception.getMessage(), exception);
        } else {
            logger.info("action=exceptionHandler error={} result={}", status, exception.getMessage());
        }
    }

    private ErrorDto logAndCreateErrorDto(HttpStatus status, Throwable exception) {
        logError(status, exception);
        return new ErrorDto(status, exception.getMessage());
    }
}
