package com.prase.mt.lab.api.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.prase.mt.lab.api.converter.PhraseAccountConverter;
import com.prase.mt.lab.api.dto.PhraseAccountCreateDto;
import com.prase.mt.lab.api.dto.PhraseAccountDto;
import com.prase.mt.lab.client.PhraseProjectDto;
import com.prase.mt.lab.service.PhraseAccountService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Phrase account")
@RestController
@RequestMapping("/phrase-demo/account")
public class PhraseAccountRestController {

    private final PhraseAccountService accountService;
    private final PhraseAccountConverter converter;

    public PhraseAccountRestController(PhraseAccountService accountService, PhraseAccountConverter converter) {
        this.accountService = accountService;
        this.converter = converter;
    }

    @Operation(
            summary = "List phrase accounts",
            tags = "Phrase account",
            description = "Returns list of all phrase accounts"
    )
    @GetMapping
    public List<PhraseAccountDto> getAllAccounts() {
        return converter.createDTOList(accountService.getAllAccounts());
    }

    @Operation(
            summary = "Phrase account detail",
            tags = "Phrase account",
            description = "Returns phrase account detail"
    )
    @GetMapping("{id}")
    public PhraseAccountDto getAccountById(@Parameter(description = "Id of phrase account",
                                                      required = true) @PathVariable Long id) {
        return converter.createDto(accountService.getAccountById(id));
    }

    @Operation(
            summary = "Creates Phrase account",
            tags = "Phrase account",
            description = "Creates new phrase account"
    )
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PhraseAccountDto createAccount(@RequestBody @Validated PhraseAccountCreateDto dto) {
        return converter.createDto(accountService.createAccount(dto.username(), dto.password()));
    }

    @Operation(
            summary = "Creates Phrase account",
            tags = "Phrase account",
            description = "Creates new phrase account"
    )
    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateAccount(@RequestBody @Validated PhraseAccountCreateDto dto, @Parameter(
            description = "Id of phrase account", required = true) @PathVariable Long id) {
        accountService.updateAccount(id, dto.username(), dto.password());
    }

    @Operation(
            summary = "Phrase account projects",
            tags = "Phrase account",
            description = "Returns phrase account projects"
    )
    @GetMapping("{id}/projects")
    public List<PhraseProjectDto> getAccountProjects(@Parameter(description = "Id of phrase account",
                                                                required = true) @PathVariable Long id,
                                                     @Parameter(
                                                             description = "List also archived projects") @RequestParam(
                                                             value = "includeArchived",
                                                             required = false) boolean includeArchived) {
        return accountService.getAccountProjects(id, includeArchived);
    }
}
