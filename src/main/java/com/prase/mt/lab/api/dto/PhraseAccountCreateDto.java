package com.prase.mt.lab.api.dto;

import jakarta.validation.constraints.NotBlank;

public record PhraseAccountCreateDto(@NotBlank String username, @NotBlank String password) {
}
