package com.prase.mt.lab.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prase.mt.lab.domain.PhraseAccountEntity;

@Repository
public interface PhraseAccountRepository extends JpaRepository<PhraseAccountEntity, Long> {

    Optional<PhraseAccountEntity> findById(Long id);
}
