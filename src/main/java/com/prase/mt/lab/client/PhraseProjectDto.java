package com.prase.mt.lab.client;

import java.util.List;

public record PhraseProjectDto(String name, String status, String sourceLang, List<String> targetLangs) {
}
