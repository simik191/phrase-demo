package com.prase.mt.lab.client.exception;

/**
 * Exception use when a phrase endpoint return exception
 */
public class PhraseGenericClientException extends RuntimeException {
    public PhraseGenericClientException(String message) {
        super(message);
    }

    public PhraseGenericClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
