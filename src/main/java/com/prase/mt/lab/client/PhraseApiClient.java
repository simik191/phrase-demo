package com.prase.mt.lab.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.prase.mt.lab.PhraseProperties;
import com.prase.mt.lab.client.exception.PhraseAccountLoginException;
import com.prase.mt.lab.client.exception.PhraseGenericClientException;
import com.prase.mt.lab.domain.PhraseAccountEntity;

@Component
public class PhraseApiClient {

    private static final Logger logger = LoggerFactory.getLogger(PhraseApiClient.class);

    private final RestTemplate restTemplate;
    private final PhraseProperties properties;

    public PhraseApiClient(RestTemplateBuilder restTemplateBuilder, PhraseProperties properties) {
        this.restTemplate = restTemplateBuilder.build();
        this.properties = properties;
    }

    /**
     * Gets phrase authorization token
     *
     * @param account Login information
     * @return Token
     * @throws PhraseAccountLoginException  If some authentication error occurred (wrong username,...)
     * @throws PhraseGenericClientException If some general error occurred (phrase web is unreachable,..)
     */
    public String getPhraseToken(PhraseAccountEntity account) {
        logger.debug("action=getPhraseToken accountId={} status=start", account.getId());
        LoginDto request = new LoginDto(account.getPassword(), account.getUsername());

        try {
            ResponseEntity<LoginResponseDto> response = restTemplate.postForEntity(properties.getUrl() + "/api2/v1/auth/login", request, LoginResponseDto.class);
            if (response.getBody() == null) {
                throw new PhraseGenericClientException("Phrase account login did not send token");
            }
            logger.debug("action=getPhraseToken accountId={} status=end", account.getId());
            return response.getBody().token();
        } catch (HttpClientErrorException.Unauthorized e) {
            throw new PhraseAccountLoginException("Incorrect login information", e);
        } catch (HttpClientErrorException.TooManyRequests e) {
            throw new PhraseAccountLoginException("Too many request have been made. You should slow down a bit", e);
        } catch (RestClientException e) {
            throw new PhraseGenericClientException("Phrase account login failed", e);
        }
    }

    /**
     * Gets all project for account
     *
     * @param token           Authorization token
     * @param includeArchived List also archived projects
     * @return All projects
     * @throws PhraseGenericClientException If some general error occurred (phrase web is unreachable,..)
     */
    public List<PhraseProjectDto> getAllProjects(String token, boolean includeArchived) {
        String tokenPart = StringUtils.rightPad(StringUtils.substring(token, 0, 5), 10, "*");
        logger.debug("action=getAllProjects token={} status=start", tokenPart);
        List<PhraseProjectDto> projectDtos = new ArrayList<>();

        HttpEntity<Void> http = createHttpEntityWithHeaders(token);
        ResponseEntity<PhraseProjectPageDto> response;
        int pageIndex = 0;
        do {
            logger.debug("action=getAllProjects token={} status=page_start pageIndex={}", tokenPart, pageIndex);
            try {
                UriComponents build = UriComponentsBuilder.fromUriString(properties.getUrl() + "/api2/v1/projects")
                        .queryParam("pageNumber", pageIndex)
                        .queryParam("pageSize", properties.getProjectPageSize())
                        .queryParam("includeArchived", includeArchived)
                        .build();
                response = restTemplate.exchange(build.toUri(), HttpMethod.GET, http, PhraseProjectPageDto.class);
                if (response.getBody() == null) {
                    logger.debug("action=getAllProjects token={} status=page_error pageIndex={} error=empty_body", tokenPart, pageIndex);
                    throw new PhraseGenericClientException("Phrase project list did not send data");
                }

                List<PhraseProjectDto> content = Optional.ofNullable(response.getBody().content())
                        .orElse(Collections.emptyList());
                projectDtos.addAll(content);
                logger.debug("action=getAllProjects token={} status=page_done pageIndex={} pageProjects={}",
                        tokenPart, pageIndex, content.size());
            } catch (HttpClientErrorException.Forbidden e) {
                throw new PhraseAccountLoginException("This phrase account has insufficient permission", e);
            } catch (HttpClientErrorException.Unauthorized e) {
                //Shouldn't happen because the phrase token has at least 24h expiration,
                // but we could improve this behavior if it starts to occur.
                throw new PhraseAccountLoginException("Your phrase token has expired. Please try it again", e);
            } catch (RestClientException e) {
                throw new PhraseGenericClientException("Phrase list project failed", e);
            }
            pageIndex++;
        } while (pageIndex < response.getBody().totalPages());
        logger.debug("action=getAllProjects token={} status=end projectCount={}", tokenPart, projectDtos.size());
        return projectDtos;
    }

    private static HttpEntity<Void> createHttpEntityWithHeaders(String token) {
        MultiValueMap<String, String> authorization = new HttpHeaders();
        authorization.put("Authorization", List.of("ApiToken " + token));
        authorization.put("User-Agent", List.of("Test application petr.simek91"));
        return new HttpEntity<>(authorization);
    }

    public record LoginResponseDto(String token) {
    }

    public record LoginDto(String password, String userName) {
    }

    public record PhraseProjectPageDto(int totalElements, int totalPages, int pageNumber,
                                       List<PhraseProjectDto> content) {
    }
}
