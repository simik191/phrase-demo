package com.prase.mt.lab.client.exception;

/**
 * Exception use when phrase login endpoint return 401, 422 status
 */
public class PhraseAccountLoginException extends RuntimeException {
    public PhraseAccountLoginException(String message, Throwable cause) {
        super(message, cause);
    }
}
