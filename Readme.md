# Home Test from Phrase
## Description
### Assignment
* Create Spring Boot application containing backend REST services required for the fictive Web Client
* The Web Client is about having 2 pages:
### Setup Page
* Phrase TMS account can be configured here.
  * The configuration should be represented as a Spring entity class.
  * Two text fields, one for username and one for password.
  * Configuration can be edited and must be saved on a persistent storage (H2 in-memory database, for example).
  * No need to care about the security of a password.
### Projects Page
* List projects retrieved from https://cloud.memsource.com/web/docs/api#operation/listProjects 
  * You will need https://cloud.memsource.com/web/docs/api#operation/login to login first using the credentials provided via the first page
* Name, status, source language and target languages should be displayed
  * You will need to implement an endpoint in your application that will provide the data for the web client
### Requirements
You should create production quality code.
   Besides the functional requirements described above, please try to show that you:
* Understand REST.
* Can work with Git by providing us your solution as a link to your Git repo.
  * Provide instructions on how to compile and run.

## Instruction for testing
### How to start application
You can use docker or plain java from your computer to run the application.
#### Docker 
You can download docker image from gitLag registry or build it locally.
* docker pull registry.gitlab.com/simik191/phrase-demo:latest
* docker build --tag registry.gitlab.com/simik191/phrase-demo:latest .

Then you can start application
* docker run -p 8000:8000 -v $PWD/test:/data registry.gitlab.com/simik191/phrase-demo:latest
#### Java
* ./mvnw build  -DskipTests
* java -Dphrase.data-path=$PWD/tt -jar target/mt-lab-demo-0.0.1-SNAPSHOT.jar 

### How to test application
For testing, you can use [swagger ui](http://localhost:8000/swagger-ui/index.html#/ ) or the ready-made [Postman collection](postman) 